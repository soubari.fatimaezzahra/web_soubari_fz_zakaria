package test

import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import org.apache.commons.lang.RandomStringUtils

import static org.springframework.http.HttpStatus.*

@Secured(['ROLE_ADMIN'])
class AnnonceController {

    AnnonceService annonceService
    UploadService uploadService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    @Secured(['ROLE_ADMIN','ROLE_MODO','ROLE_CLIENT'])
    def index(Integer max) {
        println("ggggggg")
        params.max = Math.min(max ?: 10, 100)
        respond annonceService.list(params), model:[annonceCount: annonceService.count()]
    }

    @Secured(['ROLE_ADMIN','ROLE_MODO','ROLE_CLIENT'])
    def admin() {
        respond new Annonce(params), model: [userList: User.list()]
    }

    @Secured(['ROLE_ADMIN','ROLE_MODO','ROLE_CLIENT'])
    def show(Long id) {
        respond annonceService.get(id)
    }

    def create() {
            respond new Annonce(params), model: [userList: User.list()]
    }

    def save(Annonce annonce) {
        if (annonce == null) {
            notFound()
            return
        }

//        Handle upload here
        uploadService.doUpload()

        try {
            annonceService.save(annonce)
        } catch (ValidationException e) {
            respond annonce.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'annonce.label', default: 'Annonce'), annonce.id])
                redirect annonce
            }
            '*' { respond annonce, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond annonceService.get(id)
    }

    def update(Annonce annonce) {
        if (annonce == null) {
            notFound()
            return
        }

        try {
            annonceService.save(annonce)
        } catch (ValidationException e) {
            respond annonce.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'annonce.label', default: 'Annonce'), annonce.id])
                redirect annonce
            }
            '*'{ respond annonce, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        annonceService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'annonce.label', default: 'Annonce'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'annonce.label', default: 'Annonce'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    def uploadIllustration()
    {
        def picData = request.getFile('file')
        String randomString = RandomStringUtils.random(16, true, true)
        def file = new File(grailsApplication.config.assets.path+randomString)
        while (file.exists())
        {
            randomString = RandomStringUtils.random(16, true, true)
            file = new File(grailsApplication.config.assets.path+randomString)
        }
        picData.transferTo(file)
        def annonceInstance = Annonce.get(params.id)
        annonceInstance.addToIllustrations(new Illustration(filename: randomString))
        annonceInstance.save(flush:true)
        def ret = [name: randomString, src:grailsApplication.config.assets.url+randomString]
        render ret as JSON
    }
}

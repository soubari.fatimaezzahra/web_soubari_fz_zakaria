package test

import grails.converters.JSON
import grails.converters.XML
import grails.plugin.springsecurity.annotation.Secured

@Secured('ROLE_ADMIN')
class ApiController {
//    Accessible sur /api/user/**
//    Gestion de : GET / PUT / PATCH / DELETE
    def user() {
        switch (request.getMethod()) {
            case "GET":
                if (!params.id)
                    return response.status = 400
                def userInstance = User.get(params.id)
                if (!userInstance)
                    return response.status = 404
                renderThis(userInstance, request.getHeader('Accept'))
                break;
            case "PUT":
                break;
            case "PATCH":
                break;
            case "DELETE":
                break;
            default:
                return response.status = 405
                break;
        }
    }

//    Accessible sur /api/users/**
//    Gestion de : GET / POST
    def users() {
        switch (request.getMethod()) {
            case "GET":
                break;
            case "POST":
                respond([message: "quelque chose a foiré"], status: 400)
//                render(status: 400, text: "quelque chose a foiré !")
                /*
                    0'. Vérification des paramètres : 400 (request.JSON.)
                    1. On crée un nouvel utilisateur à partir des données du body (username, password)
                    1'. Vérifier que l'utilisateur se sauvegarde bien : 400
                    2. On récupère le role (role)
                    2'. Le role n'existe pas : 404
                    3. Associer le role à l'utilisateur que l'on vient de créer
                    3'. Vérifier que l'attribution du rôle s'est bien passée : 400
                    4. Sauvegarder le tout
                    4'. Sauvegarde échoue : 400
                    5. Renvoyer un code d'état pour informer le client que l'utilisateur a bien été créé : 201
                    6. Renvoyer l'instance de l'utilisateur créé
                 */
                break;
            default:
                return response.status = 405
                break;
        }
    }

    def annonce() {

        switch (request.getMethod()) {
            case "GET":
                break;
            case "PUT":
                break;
            case "PATCH":
                break;
            case "DELETE":
                break;
            default:
                return response.status = 405
                break;
        }
    }

    def annonces() {
        switch (request.getMethod()) {
            case "GET":
                break;
            case "POST":
                /*
                Envoyer data :
                1) Convertir la donnée binaire en donnée base64 (augmentation d'environ 30%)
                2) Passer par un envoi en multipart/form-data au lieu d'envoyer la donnée en JSON (ne fonctionne qu'en POST)

                Quand envoyer la data :
                1) Envoyer les données de la / des images en même temps que les données de l'annonce
                2) Créer l'annonce sans illustration et passer par l'update d'annonce pour ajouter les illustrations
                 */
                break;
            default:
                return response.status = 405
                break;
        }
    }

    def renderThis(Object instance, String acceptHeader) {
        switch (acceptHeader) {
            case "json":
            case "application/json":
            case "text/json":
                render instance as JSON
                return
                break;
            case "xml":
            case "application/xml":
            case "text/xml":
                render instance as XML
                return
                break;
            default:
                return response.status = 406
                break;
        }
    }
}

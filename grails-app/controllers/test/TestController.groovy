package test

class TestController {

    def doSomething() {
        render (view: '/test', model: [annonceList: Annonce.list()])
    }
}

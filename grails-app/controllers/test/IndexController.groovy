package test

class IndexController {

    def index() {
        def annonces = Annonce.list(max: null, sort: "dateCreated", order: "desc")
        render( view:'/index', model: [data: annonces])
    }
}

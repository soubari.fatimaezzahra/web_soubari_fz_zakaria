<!doctype html>
<html>
<header class="text-center text-white" style="background-color: #caced1;">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <nav class="navbar navbar-expand-lg fixed-top sticky-top navbar-dark bg-dark text-center text-white">
        <!-- Container wrapper -->
        <div class="container-fluid">
            <!-- Toggle button -->
            <button
                    class="navbar-toggler"
                    type="button"
                    data-mdb-toggle="collapse"
                    data-mdb-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent"
                    aria-expanded="false"
                    aria-label="Toggle navigation"
            >
                <i class="fas fa-bars"></i>
            </button>

            <!-- Collapsible wrapper -->
            <div class="collapse navbar-collapse" id="navbarSupportedContent">

                <!-- Left links -->
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link" href="#">Home</a>
                    </li>
                    <sec:ifLoggedIn>
                    <li class="nav-item">
                    <g:link controller="annonce" action="index"><span class="text-decoration-none nav-link"> Administration </span></g:link>
                    </li>
                    </sec:ifLoggedIn>
                </ul>
                <!-- Left links -->
            </div>
            <!-- Collapsible wrapper -->
            <!-- Right elements -->
            <div class="d-flex align-items-center">
                <ul class="navbar-nav m-2 p-2 me-auto mb-2 mb-lg-0">
                    <sec:ifLoggedIn>
                        <li class="nav-item"><span class="nav-link">Bonjour <sec:loggedInUserInfo field="username"/></span></li>
                        <li class="nav-item"><g:link controller="logout" action="index"><span class="text-decoration-none nav-link">Logout</span></g:link></li>

                    </sec:ifLoggedIn>
                    <sec:ifNotLoggedIn>
                        <li class="nav-item"><a class="nav-link" href="${createLink(controller:'login')}">Se connecter</a></li>
                    </sec:ifNotLoggedIn>
                </ul>
            </div>
            <!-- Right elements -->
        </div>
        <!-- Container wrapper -->
    </nav>
    <!-- Navbar -->
    <!-- Background image -->
    <!-- image -->
    <div
            class="p-5 text-center bg-image"
            style="
            background-image: url('https://www.hammer-fitness.ch/ch-fr/media/content/startseite/imagebild-kraft-ausdauer.jpg');
            height: 600px;
            "
    >
    <!--  PICTURE -->
        <div class="mask p-5" style="background-color: rgba(0, 0, 0, 0.6);">
            <div class="d-flex justify-content-center align-items-center h-100">
                <div class="text-white">
                    <h1 class="mb-3">Bienvenue au BoinCoin</h1>

                    <g:form resource="${this.annonce}" method="GET" action="searchAnnonce" controller="homepage">
                        <div class="container-fluid">
                            <form class="d-flex input-group w-auto">
                                <input
                                        type="search"
                                        class="form-control rounded"
                                        placeholder="Search"
                                        aria-label="Search"
                                        aria-describedby="search-addon"
                                />
                            </form>
                        </div>
                    </g:form>
                </div>
            </div>
        </div>
    </div>
    <!-- Background image -->
</header>

<body>
<section class="align-content-center">
    <h2 class="p-2 text-center">Annonces publiées</h2>
     <div class="row align-content-center m-3 p-2">
              <div class="card p-2 m-2" style="width: 18rem;">


                  <asset:image src="v1.jpg" alt="Grails Logo"/>

                  <div class="card-body">
                      <p class="card-text"> TAPIS DE COURSE <br>
                          Une expérience fitness unique, et des séances plus intenses avec la série Incline Trainers. Leur capacité exceptionnelle de déclinaison de -6% ainsi que leur inclinaison jusqu'à 40%, rendent les entraînements aussi intenses qu'efficaces et les calories brûlent à toute vitesse. Enrichie du coaching interactif iFIT avec des professionnels qui vous accompagnent et qui interagissent sur votre effort, l'expérience d'entraînement sur Incline Trainers est unique en son genre.</p>
                  </div>

              </div>
              <div class="card p-2 m-2" style="width: 18rem;">


                  <asset:image src="v9.jpg" alt="Grails Logo"/>

                  <div class="card-body">
                      <p class="card-text"> VÉLO D’APPARTEMENT
                          L'expérience fitness, cycliste et cross training immersive par excellence. Les écrans rotatifs faciliteront les transitions entre les mouvements sur et hors du vélos pendant les cours collectifs iFIT. Vous aurez aussi la possibilité de reproduire des segments de route les plus prestigieux avec une inclinaison et une déclinaison sur cette série Studio jusqu'à 20%, c'est comme si vous y étiez !.</p>
                  </div>

              </div>


         <div class="card p-2 m-2" style="width: 18rem;">


             <asset:image src="v14.PNG" alt="Grails Logo"/>

             <div class="card-body">
                 <p class="card-text"> Fusion CST <br>
                 Travaillez cardio et force simultanément. Le Fusion CST changera votre façon de vous entraîner vous n'aurez qu'à suivre les mouvement des coachs iFIT à l'aide de la tablette NordicTrack Portal 10". Avec la diversité des vidéos d'entraînements iFIT, cet équipement apportera une réelle valeur ajoutée dont vous ne pourrez plus vous passer..</p>
             </div>
         </div>

             <div class="card p-2 m-2" style="width: 18rem;">


                 <asset:image src="v16.jpg" alt="Grails Logo"/>

                 <div class="card-body">
                     <p class="card-text"> Elliptiques Freestride Trainers <br><br>
                         Faites l'expérience d'un entraînement interactif sur mesure grâce à notre série de vélos elliptiques Freestride Trainers. Dotés d'écrans tactiles HD allant jusqu'à 10" et d'une rampe inclinable maximale de -10° à +10°, obtenez les bénéfices d'une machine 3 en 1 pour des résultats concrets ! Le Freestride Trainer combine les mouvements du tapis de course, de l'elliptique et du stepper..</p>
                 </div>
             </div>


         <div class="card p-2 m-2" style="width: 18rem;">


             <asset:image src="v17.jpg" alt="Grails Logo"/>

             <div class="card-body">
                 <p class="card-text"> Presse à cuisse ou Hack Squat <br>
                     La construction en acier tubulaire de la presse à cuisses 2 en 1 MAXXUS impressionne par son excellente fabrication et sa très grande stabilité. L'entraînement avec des poids lourds exige un maximum de stabilité et une masse élevée afin de pouvoir supporter le poids et éviter tout risque de torsion.

                     L'entraînement avec cette presse MAXXUS est très efficace car tu peux travailler avec un poids suffisamment lourd pour stimuler la croissance musculaire. La combinaison entre presse à jambes et machine de Hackenschmidt est optimale pour un entraînement complet des jambes et des cuisses.

                     La conversion entre les 2 modes d'entraînements ne prend que quelques secondes. Les temps de pause sont ainsi réduits au minimum.</p>
             </div>
         </div>


         <div class="card p-2 m-2" style="width: 18rem;">


             <asset:image src="v20.PNG" alt="Grails Logo"/>

             <div class="card-body">
                 <p class="card-text"> IlLesTonal <br>
                     ilLesTonal est une machine murale qui a deux bras réglables ; vous pouvez les déplacer de haut en bas et les incliner pour divers exercices de poussée ou de traction. Les poignées peuvent également être échangées contre deux poignées, une barre ou une corde. Certaines de ces poignées comportent un bouton marche/arrêt qui permet de se mettre en position avant de démarrer la musculation. Le kit de démarrage comprend également un banc et un tapis de sol..</p>
             </div>
         </div>

         <div class="card p-2 m-2" style="width: 18rem;">


             <asset:image src="v21.jpg" alt="Grails Logo"/>

             <div class="card-body">
                 <p class="card-text"> BODY-SOLID VERTICAL DIPS ABDOS TRACTION STATION FCD <br><br>
                     Cet appareil Dips/Abdo/Traction Station 3 en 1 de la marque Body Solid se présente sous un magnifique châssis acier au profile ovalisé, la géométrie du châssis assure une excellente stabilité, Cette station performante et haut de gamme, rassemble à elle seule plusieurs possibilités d’exercices avec tout le confort recherché. Bien positionner, correctement calé, avec un excellent maintien des lombaires pendant votre séance pour les abdominaux en relevé de jambes ou en traction pour les dorsaux…quelques soit l’exercice choisi, vous serez toujours prêt pour commencer votre séance Fitness dans les meilleures conditions possibles !

                     La sellerie breveté DuraFirm de l’appui du bas du dos et des accoudoirs réduisent considérablement la fatigue musculaire et l'inconfort du à de longue séances en appui sur les avant-bras. Les accoudoirs surélevés calent parfaitement votre dos et vous permettent ainsi de vous concentrer sur le travail de la sangle abdominale en relevé de jambes.</p>
             </div>
         </div>


         <div class="card p-2 m-2" style="width: 18rem;">


             <asset:image src="v23.jpg" alt="Grails Logo"/>

             <div class="card-body">
                 <p class="card-text"> Sit-Up GHD <br><br>
                     Le sit-up fessier-jambon-développeur est un mouvement incroyable qui nécessite et développe une force de base énorme à travers la partie avant du corps. L'appareil GHD permet une plage de mouvement unique et dynamique qui est très difficile à recréer sur n'importe quel autre appareil d'entraînement.

                     C'est un mouvement très puissant qui prend du temps à se développer. Ne mordez pas plus que ce que vous pouvez mâcher avant d'être prêt… sinon, vos abdominaux seront douloureux pendant une semaine et vous ne pourrez peut-être pas vous asseoir hors du lit.</p>
             </div>
         </div>

     </div>

</section>

</body>
<!-- Section: Images city -->
<footer class="text-center bg-dark text-center text-white">
    <!-- Grid container -->
    <div class="container p-4">
        <!-- Section: Images -->
        <section class="">
            <div class="row">
                <div class="col-lg-2 col-md-12 mb-4 mb-md-0">
                    <div
                            class="bg-image hover-overlay ripple shadow-1-strong rounded"
                            data-ripple-color="light"
                    >
                        <img
                                src="https://res.cloudinary.com/iconfitness/image/upload/w_690,h_459,c_fill,ar_230:153,dpr_1,f_auto,q_auto/v1/site--18/netl32719_galleryf.jpg"
                                class="w-100"
                        />
                        <a href="#!">
                            <div
                                    class="mask"
                                    style="background-color: rgba(251, 251, 251, 0.2);"
                            ></div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-12 mb-4 mb-md-0">
                    <div
                            class="bg-image hover-overlay ripple shadow-1-strong rounded"
                            data-ripple-color="light"
                    >
                        <img
                                src="https://res.cloudinary.com/iconfitness/image/upload/w_690,h_459,c_fill,ar_230:153,dpr_1,f_auto,q_auto/v1/site--18/netl32719_galleryc.jpg"
                                class="w-100"
                        />
                        <a href="#!">
                            <div
                                    class="mask"
                                    style="background-color: rgba(251, 251, 251, 0.2);"
                            ></div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-12 mb-4 mb-md-0">
                    <div
                            class="bg-image hover-overlay ripple shadow-1-strong rounded"
                            data-ripple-color="light"
                    >
                        <img
                                src="https://res.cloudinary.com/iconfitness/image/upload/w_690,h_459,c_fill,ar_230:153,dpr_1,f_auto,q_auto/v1/site--18/netl32719_galleryb.jpg"
                                class="w-100"
                        />
                        <a href="#!">
                            <div
                                    class="mask"
                                    style="background-color: rgba(251, 251, 251, 0.2);"
                            ></div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-12 mb-4 mb-md-0">
                    <div
                            class="bg-image hover-overlay ripple shadow-1-strong rounded"
                            data-ripple-color="light"
                    >
                        <img
                                src="https://res.cloudinary.com/iconfitness/image/upload/w_690,h_459,c_fill,ar_230:153,dpr_1,f_auto,q_auto/v1/site--18/New-fs7i-gallery-d.jpg"
                                class="w-100"
                        />
                        <a href="#!">
                            <div
                                    class="mask"
                                    style="background-color: rgba(251, 251, 251, 0.2);"
                            ></div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-12 mb-4 mb-md-0">
                    <div
                            class="bg-image hover-overlay ripple shadow-1-strong rounded"
                            data-ripple-color="light"
                    >
                        <img
                                src="https://cdn.vox-cdn.com/thumbor/EqgHu9uMBUZoJQ1PZyzZz6mSQUg=/0x0:2040x1360/1120x0/filters:focal(0x0:2040x1360):format(webp):no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/18352950/akrales_190725_3568_0063.jpg"
                                class="w-100"
                        />
                        <a href="#!">
                            <div
                                    class="mask"
                                    style="background-color: rgba(251, 251, 251, 0.2);"
                            ></div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-12 mb-4 mb-md-0">
                    <div
                            class="bg-image hover-overlay ripple shadow-1-strong rounded"
                            data-ripple-color="light"
                    >
                        <img
                                src="https://cdn.vox-cdn.com/thumbor/wN-A1GV5BV_OJPvZ0FOxiccsSgU=/0x0:2040x1360/1920x0/filters:focal(0x0:2040x1360):format(webp):no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/18352955/akrales_190725_3568_0138.jpg"
                                class="w-100"
                        />
                        <a href="#!">
                            <div
                                    class="mask"
                                    style="background-color: rgba(251, 251, 251, 0.2);"
                            ></div>
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <!-- Section: Images -->
    </div>
    <!-- Grid container -->

    <!-- Copyright -->
    <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
        © 2021 SOUBARI__Fatima-ezzahra _&& _Zakaria __BIHAR:
        <a class="text-white" href="/">BonCoin</a>
    </div>
    <!-- Copyright -->
</footer>
</html>

<%--
  Created by IntelliJ IDEA.
  User: fatima.soubari
  Date: 26/11/2021
  Time: 04:56--%>


<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html>
<header class="text-center text-white" style="background-color: #caced1;">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'annonce.label', default: 'Annonce')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<a href="#list-annonce" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                              default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label"
                                                              args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="list-annonce" class="content scaffold-list" role="main">
    <h1><g:message code="default.list.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>

    <table>
        <thead>
        <tr>

            <th class="sortable"><a href="/annonce/index?sort=title&amp;max=10&amp;order=asc">Title</a></th>

            <th class="sortable"><a href="/annonce/index?sort=description&amp;max=10&amp;order=asc">Description</a></th>

            <th class="sortable"><a href="/annonce/index?sort=price&amp;max=10&amp;order=asc">Price</a></th>

            <th class="sortable"><a href="/annonce/index?sort=illustrations&amp;max=10&amp;order=asc">Illustrations</a></th>

            <th class="sortable"><a href="/annonce/index?sort=author&amp;max=10&amp;order=asc">Author</a></th>

        </tr>
        </thead>
        <tbody>

        <g:each in="${annonceList}" var="annonce">

            <tr class="even">

                <td>
                    <g:link controller="annonce" action="show" id="${annonce.id}">
                        ${annonce.title}
                    </g:link>
                </td>



                <td>${annonce.description}</td>



                <td>${annonce.price}</td>



                <td>
                    <g:each in="${annonce.illustrations}" var="illustration">
                        <img src="${grailsApplication.config.assets.url + illustration.filename}"/>
                    </g:each>
                </td>



                <td>
                    <g:link controller="user" action="show" id="${annonce.author.id}">
                        ${fieldValue(bean: annonce, field: 'author.username')}
                    </g:link>
                </td>

            </tr>

        </g:each>
        </tbody>
    </table>

    <div class="pagination">
        <g:paginate total="${annonceCount ?: 0}"/>
    </div>
</div>
</body>
<footer class="text-center bg-dark text-center text-white">
    <!-- Grid container -->
    <div class="container p-4">
        <!-- Section: Images -->
        <section class="">
            <div class="row">
                <div class="col-lg-2 col-md-12 mb-4 mb-md-0">
                    <div
                            class="bg-image hover-overlay ripple shadow-1-strong rounded"
                            data-ripple-color="light"
                    >
                        <img
                                src="https://res.cloudinary.com/iconfitness/image/upload/w_690,h_459,c_fill,ar_230:153,dpr_1,f_auto,q_auto/v1/site--18/netl32719_galleryf.jpg"
                                class="w-100"
                        />
                        <a href="#!">
                            <div
                                    class="mask"
                                    style="background-color: rgba(251, 251, 251, 0.2);"
                            ></div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-12 mb-4 mb-md-0">
                    <div
                            class="bg-image hover-overlay ripple shadow-1-strong rounded"
                            data-ripple-color="light"
                    >
                        <img
                                src="https://res.cloudinary.com/iconfitness/image/upload/w_690,h_459,c_fill,ar_230:153,dpr_1,f_auto,q_auto/v1/site--18/netl32719_galleryc.jpg"
                                class="w-100"
                        />
                        <a href="#!">
                            <div
                                    class="mask"
                                    style="background-color: rgba(251, 251, 251, 0.2);"
                            ></div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-12 mb-4 mb-md-0">
                    <div
                            class="bg-image hover-overlay ripple shadow-1-strong rounded"
                            data-ripple-color="light"
                    >
                        <img
                                src="https://res.cloudinary.com/iconfitness/image/upload/w_690,h_459,c_fill,ar_230:153,dpr_1,f_auto,q_auto/v1/site--18/netl32719_galleryb.jpg"
                                class="w-100"
                        />
                        <a href="#!">
                            <div
                                    class="mask"
                                    style="background-color: rgba(251, 251, 251, 0.2);"
                            ></div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-12 mb-4 mb-md-0">
                    <div
                            class="bg-image hover-overlay ripple shadow-1-strong rounded"
                            data-ripple-color="light"
                    >
                        <img
                                src="https://res.cloudinary.com/iconfitness/image/upload/w_690,h_459,c_fill,ar_230:153,dpr_1,f_auto,q_auto/v1/site--18/New-fs7i-gallery-d.jpg"
                                class="w-100"
                        />
                        <a href="#!">
                            <div
                                    class="mask"
                                    style="background-color: rgba(251, 251, 251, 0.2);"
                            ></div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-12 mb-4 mb-md-0">
                    <div
                            class="bg-image hover-overlay ripple shadow-1-strong rounded"
                            data-ripple-color="light"
                    >
                        <img
                                src="https://cdn.vox-cdn.com/thumbor/EqgHu9uMBUZoJQ1PZyzZz6mSQUg=/0x0:2040x1360/1120x0/filters:focal(0x0:2040x1360):format(webp):no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/18352950/akrales_190725_3568_0063.jpg"
                                class="w-100"
                        />
                        <a href="#!">
                            <div
                                    class="mask"
                                    style="background-color: rgba(251, 251, 251, 0.2);"
                            ></div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-12 mb-4 mb-md-0">
                    <div
                            class="bg-image hover-overlay ripple shadow-1-strong rounded"
                            data-ripple-color="light"
                    >
                        <img
                                src="https://cdn.vox-cdn.com/thumbor/wN-A1GV5BV_OJPvZ0FOxiccsSgU=/0x0:2040x1360/1920x0/filters:focal(0x0:2040x1360):format(webp):no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/18352955/akrales_190725_3568_0138.jpg"
                                class="w-100"
                        />
                        <a href="#!">
                            <div
                                    class="mask"
                                    style="background-color: rgba(251, 251, 251, 0.2);"
                            ></div>
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <!-- Section: Images -->
    </div>
    <!-- Grid container -->

    <!-- Copyright -->
    <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
        © 2021 SOUBARI__Fatima-ezzahra _&& _Zakaria __BIHAR:
        <a class="text-white" href="/">BonCoin</a>
    </div>
    <!-- Copyright -->
</footer>
</html>
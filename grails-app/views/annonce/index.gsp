<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admin | BonCoin</title>

    <!-- Google Font: Source Sans Pro -->
    <asset:stylesheet href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback"/>
    <!-- Font Awesome -->
    <asset:stylesheet href="../../assets/admin/plugins/fontawesome-free/css/all.min.css"/>
    <!-- Theme style -->
    <asset:stylesheet href="../../assets/admin/dist/css/adminlte.min.css"/>
    <!-- overlayScrollbars -->
    <asset:stylesheet href="../../assets/admin/plugins/overlayScrollbars/css/OverlayScrollbars.min.css"/>
</head>

<body class="hold-transition sidebar-mini layout-fixed" data-panel-auto-height-mode="height">
<div class="wrapper">

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="/" class="nav-link">Home</a>
            </li>
        </ul>

        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">

            <li class="nav-item">
                <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                    <i class="fas fa-expand-arrows-alt"></i>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
                    <i class="fas fa-th-large"></i>
                </a>
            </li>
        </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <li><a href="${createLink(uri: '/')}">
            <span class="brand-text font-weight-light">BonCoin</span></a></li>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="info">
                    <a href="#" class="d-block"> <sec:loggedInUserInfo field="username"/></a>
                </div>
            </div>
            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
                    <li class="nav-item">
                        <a href="/illustration/index" class="nav-link">
                            <i class="nav-icon fas fa-tree"></i>
                            <p>
                                Illustrations
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/user/index" class="nav-link">
                            <i class="nav-icon fas fa-edit"></i>
                            <p>
                                Utilisateurs
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/annonce/index" class="nav-link">
                            <i class="nav-icon fas fa-table"></i>
                            <p>
                                Annonces
                            </p>
                        </a>
                    </li>
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->

    </aside>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper iframe-mode" data-widget="iframe" data-loading-screen="750">
        <div class="nav navbar navbar-expand navbar-white navbar-light border-bottom p-0">
            <div class="nav-item dropdown">
                <a class="nav-link bg-danger dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Close</a>
                <div class="dropdown-menu mt-0">
                    <a class="dropdown-item" href="#" data-widget="iframe-close" data-type="all">Close All</a>
                    <a class="dropdown-item" href="#" data-widget="iframe-close" data-type="all-other">Close All Other</a>
                </div>
            </div>
            <a class="nav-link bg-light" href="#" data-widget="iframe-scrollleft"><i class="fas fa-angle-double-left"></i></a>
            <ul class="navbar-nav overflow-hidden" role="tablist"></ul>
            <a class="nav-link bg-light" href="#" data-widget="iframe-scrollright"><i class="fas fa-angle-double-right"></i></a>
            <a class="nav-link bg-light" href="#" data-widget="iframe-fullscreen"><i class="fas fa-expand"></i></a>
        </div>
    <!-- /.card-body -->
    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title">Annonces</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <button type="button" class="btn btn-block btn-outline-primary btn-lg"><g:link class="annonce" action="create">+ Nouvel Annonce</g:link></button>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th class="sortable"><a href="/annonce/index?sort=title&amp;max=10&amp;order=asc">Title</a></th>

                    <th class="sortable"><a href="/annonce/index?sort=description&amp;max=10&amp;order=asc">Description</a></th>

                    <th class="sortable"><a href="/annonce/index?sort=price&amp;max=10&amp;order=asc">Price</a></th>

                    <th class="sortable">Illustrations</th>

                    <th class="sortable"><a href="/annonce/index?sort=author&amp;max=10&amp;order=asc">Author</a></th>

                    <th class="sortable">Actions</th>
                </tr>
                </thead>
                <tbody>
                <g:each in="${annonceList}" var="annonce">
                    <tr>
                        <td><g:link controller="annonce" action="show" id="${annonce.id}">
                            ${annonce.title}
                        </g:link></td>
                        <td>
                            ${annonce.description}
                        </td>
                        <td>${annonce.price}</td>
                        <td>
                            <g:each in="${annonce.illustrations}" var="illustration">
                                <img src="${grailsApplication.config.assets.url + illustration.filename}"/>
                            </g:each>
                        </td>
                        <td>
                            <g:link controller="user" action="show" id="${annonce.author.id}">
                                ${fieldValue(bean: annonce, field: 'author.username')}
                            </g:link>
                        </td>
                        <td>
                            <button type="button" class="btn btn-outline-warning btn-flat btn_mod_ann" title="Editer l'annonce"><i class="fa fa-edit"></i>
                                <g:link controller="annonce" action="edit" id="${annonce.id}"><g:message code="default.button.edit.label" default="Edit"/></g:link>
                            </button>
                            <button type="button" class="btn btn-outline-danger btn-flat btn_sup_ann" title="Supprimer l'annonce"><i class="fa fa-trash"></i></button>
                        </td>
                    </tr>
                </g:each>
                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
        <div class="card-footer clearfix">
            <ul class="pagination-sm m-0 float-right">

                <g:paginate total="${annonceCount ?: 0}"/>

            </ul>
        </div>
    </div>
    <!-- /.card -->
</div>
    <footer class="main-footer">
        <strong>Copyright © 2021 SOUBARI__Fatima-ezzahra _&& _Zakaria __BIHAR:</strong>
                            grails projet
        <div class="float-right d-none d-sm-inline-block">
            <b>Version</b> 3.1.0
        </div>
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
</div>
    <!-- ./wrapper -->

    <!-- jQuery -->
    <asset:script src="../../assets/admin/plugins/jquery/jquery.min.js"/>
    <!-- jQuery UI 1.11.4 -->
    <asset:script src="../../assets/admin/plugins/jquery-ui/jquery-ui.min.js"/>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <asset:script>
        $.widget.bridge('uibutton', $.ui.button)
    </asset:script>
    <!-- Bootstrap 4 -->
    <asset:script src="../../assets/admin/plugins/bootstrap/js/bootstrap.bundle.min.js"/>
    <!-- overlayScrollbars -->
    <asset:script src="../../assets/admin/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"/>
    <!-- AdminLTE App -->
    <asset:script src="../../assets/admin/dist/js/adminlte.js"/>
    <!-- AdminLTE for demo purposes -->
    <asset:script src="../../assets/admin/dist/js/demo.js"/>
</body>
</html>
